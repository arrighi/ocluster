open Lwt.Infix
open Serialization_t

let max_std = 5120000 (* 5Mo *)

let current_load = ref 0.

let running_jobs = CCVector.create ()

let get_ram () =
  Lwt_io.lines_of_file "/proc/meminfo"
  |> Lwt_stream.find_map (fun line ->
         if CCString.prefix ~pre:"MemAvailable" line then
           match String.split_on_char ':' line with
           | [ _; ram ] ->
               Option.bind
                 (String.split_on_char ' ' (String.trim ram) |> CCList.head_opt)
                 CCInt.of_string
           | _ -> None
         else None)

let get_stat () =
  let re = Str.regexp {|^cpu[0-9][0-9]* \(.*\)|} in
  Lwt_io.lines_of_file "/proc/stat"
  |> Lwt_stream.filter_map (fun line ->
         if Str.string_match re line 0 then
           Str.matched_group 1 line |> String.split_on_char ' '
           |> CCList.foldi
                (fun (sum, idle) i e ->
                  let e = int_of_string e in
                  (sum + e, if i = 3 then e else idle))
                (0, 0)
           |> CCOpt.return
         else None)
  |> Lwt_stream.to_list

let rec update_load stat_fst =
  Lwt_unix.sleep 1. >>= fun () ->
  get_stat () >>= fun stat ->
  current_load :=
    CCList.fold_left2
      (fun load (total_fst, idle_fst) (total_snd, idle_snd) ->
        load
        +. float (total_snd - total_fst - idle_snd + idle_fst)
           /. float (total_snd - total_fst))
      0. stat_fst stat;
  update_load stat

let get_loadavg () =
  Lwt_io.(with_file ~mode:input "/proc/loadavg" read_line_opt)
  >|= CCOpt.flat_map (fun s ->
          match String.split_on_char ' ' s with
          | v :: _ -> Some (CCFloat.of_string_exn v)
          | _ -> None)

let get_cpu_count () =
  Lwt_io.lines_of_file "/proc/cpuinfo"
  |> Lwt_stream.find_map (fun line ->
         if CCString.prefix ~pre:"cpu cores" line then
           match String.split_on_char ':' line with
           | [ _; ncpu ] -> String.trim ncpu |> CCInt.of_string
           | _ -> None
         else None)

let get_available_cpu () =
  get_cpu_count () >>= fun cpu_count ->
  get_loadavg () >|= fun loadavg ->
  match (cpu_count, loadavg) with
  | Some cpu_count, Some loadavg ->
      cpu_count
      - CCFloat.(ceil (CCFloat.max loadavg !current_load +. 1.7) |> to_int)
  | _ -> 0

let get_available_ram () =
  get_ram () >|= fun ram ->
  match ram with Some ram -> (ram / 1024) - 4096 | None -> 0

let process_status_to_ret_code = function
  | Unix.WEXITED c -> `WEXITED c
  | Unix.WSIGNALED s -> `WSIGNALED s
  | Unix.WSTOPPED s -> `WSTOPPED s

let run_computation (computation : computation) =
  Lwt_io.with_temp_file ~prefix:"ocluster" ~perm:0o700 (fun (name, oc) ->
      Lwt_io.write_line oc computation.script >>= fun () ->
      Lwt_io.close oc >>= fun () ->
      Lwt_unix.sleep 2. >>= fun () ->
      let read_stderr, write_stderr = Lwt_unix.pipe_in ()
      and read_stdout, write_stdout = Lwt_unix.pipe_in () in
      let%lwt ret_code =
        Lwt_process.exec ?timeout:computation.time ~stdin:`Close
          ~env:(Array.of_list computation.env)
          ~stdout:(`FD_move write_stdout) ~stderr:(`FD_move write_stderr)
          (name, Array.of_list (name :: computation.args))
      and stdout = Lwt_io.read (Lwt_io.of_fd ~mode:Lwt_io.input read_stdout)
      and stderr = Lwt_io.read (Lwt_io.of_fd ~mode:Lwt_io.input read_stderr) in
      Lwt_unix.close read_stderr >>= fun () ->
      Lwt_unix.close read_stdout >|= fun () ->
      {
        id = computation.id;
        stdout =
          ( if String.length stdout > max_std then
            CCString.drop (String.length stdout - max_std) stdout
          else stdout );
        stderr =
          ( if String.length stderr > max_std then
            CCString.drop (String.length stderr - max_std) stderr
          else stderr );
        ret_code = process_status_to_ret_code ret_code;
      })

let rec send_result sockaddr pass result =
  if%lwt
    Lwt_io.with_connection sockaddr (fun (ic, oc) ->
        Lwt_io.write_line oc
          (Serialization_j.string_of_query (pass, `RESULT result))
        >>= fun () ->
        Lwt_io.flush oc >>= fun () ->
        try%lwt
          Lwt_io.read_line ic >>= fun json ->
          match
            CCResult.guard (fun () -> Serialization_j.answer_of_string json)
          with
          | Ok answer -> (
              match answer with
              | `Ok ->
                  Logs_lwt.debug (fun m ->
                      m "Result %i,%i successfully sent." (fst result.id)
                        (snd result.id))
                  >|= fun () -> false
              | `Error s ->
                  Logs_lwt.err (fun m ->
                      m "Error during the reception of the result %i,%i: %s"
                        (fst result.id) (snd result.id) s)
                  >|= fun () -> true )
          | Error e ->
              Logs_lwt.err (fun m ->
                  m "Error during the reception of the answer: %s"
                    (Printexc.to_string e))
              >|= fun () -> true
        with End_of_file ->
          Logs_lwt.err (fun m -> m "Error during the read of the answer: EOF")
          >|= fun () -> true)
  then send_result sockaddr pass result

let handle_computation sockaddr pass computation () =
  CCVector.push running_jobs computation;
  run_computation computation >>= fun result ->
  let sockaddr =
    match sockaddr with
    | Unix.ADDR_INET (a, _) -> Unix.ADDR_INET (a, computation.port)
    | s -> s
  in
  CCVector.filter_in_place (fun (c:computation) -> c.id != computation.id) running_jobs;
  send_result sockaddr pass result
  <&> Logs_lwt.info (fun m ->
          m "End computation %i,%i" (fst computation.id) (snd computation.id))

let server_handler pass sockaddr (ic, oc) =
  Lwt_io.read_line ic >>= fun json ->
  ( match CCResult.guard (fun () -> Serialization_j.query_of_string json) with
  | Result.Ok (query_pass, query) ->
      if query_pass = pass then
        match query with
        | `COMPUTATION (computation : computation) ->
            Lwt_io.write_line oc (Serialization_j.string_of_answer `Ok)
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.debug (fun m ->
                    m "Receive computation: %s"
                      (Serialization_j.string_of_computation
                         { computation with env = []; script = "<script>" }))
            >|= fun () ->
            Lwt.async (handle_computation sockaddr pass computation)
        | `STAT ->
            get_available_cpu () >>= fun cpu_count ->
            get_available_ram () >>= fun ram ->
            Lwt_io.write_line oc
              (Serialization_j.string_of_stat (cpu_count, ram))
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.debug (fun m -> m "Receive STAT command")
        | `RUNNING ->
            Lwt_io.write_line oc
              (Serialization_j.string_of_running_jobs (CCVector.to_list running_jobs))
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.debug (fun m -> m "Receive RUNNING command")
        | _ -> Logs_lwt.warn (fun m -> m "Receive a unwanted command")
      else
        Lwt_io.write_line oc
          (Serialization_j.string_of_answer (`Error "Wrong password"))
        >>= (fun () -> Lwt_io.flush oc)
        <&> Logs_lwt.warn (fun m -> m "Wrong password: %s" query_pass)
  | Result.Error e ->
      Logs_lwt.err (fun m ->
          m "Error during the reception of the computation: %s"
            (Printexc.to_string e)) )
  >>= fun () -> Lwt_io.flush Lwt_io.stderr

let stop_server resolver server _ = Lwt.wakeup_later resolver server

let cmd config () =
  let conf =
    Serialization_j.node_conf_of_string (CCIO.with_in config CCIO.read_all)
  in
  let promise, resolver = Lwt.task () in
  Logs_lwt.info (fun m -> m "Node at port %i with pass %s" conf.port conf.pass)
  >>= fun () ->
  ( Lwt.async (fun () -> get_stat () >>= update_load);
    Lwt_io.establish_server_with_client_address
      (Unix.ADDR_INET (Unix.inet_addr_any, conf.port))
      (server_handler conf.pass) )
  >>= fun server ->
  let _ = Lwt_unix.on_signal 15 (stop_server resolver server)
  and _ = Lwt_unix.on_signal 2 (stop_server resolver server) in
  promise >>= fun server ->
  Lwt_io.shutdown_server server
  <&> Logs_lwt.info (fun m -> m "Shuting down node server")
