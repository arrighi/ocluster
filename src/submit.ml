open Lwt.Infix
open Serialization_t

let cmd cpu ram time iteration port script_file pass addr args () =
  let script = CCIO.with_in script_file CCIO.read_all in
  let submission = {name = script_file; script; args; time; iteration; cpu; ram} in
  Lwt_io.with_connection
    (Lwt_unix.ADDR_INET (Unix.inet_addr_of_string addr, port))
    (fun (ic, oc) ->
      Lwt_io.write_line oc
        (Serialization_j.string_of_query (pass, `JOB submission))
      >>= fun () ->
      Lwt_io.read ic
      >>= fun json ->
      match
        CCResult.guard (fun () -> Serialization_j.answer_of_string json)
      with
      | Ok answer -> (
        match answer with
        | `Ok ->
            Logs_lwt.info (fun m -> m "Computation successfully sent.")
        | `Error s ->
            Logs_lwt.err (fun m ->
                m "Error during the reception of the computation: %s" s) )
      | Error e ->
          Logs_lwt.err (fun m ->
              m "Error during the reception of the answer: %s"
                (Printexc.to_string e)))
