open Lwt.Infix
open Serialization_t
module SHashtbl = CCHashtbl.Make (CCString)

(* TODO: store currently running jobs and check from time
 * to time that node are still alive *)

let nodes : (int * computation list) SHashtbl.t = SHashtbl.create 10

let jobs : job CCDeque.t = CCDeque.create ()

let jobs_id = ref 0

let string_of_ret_code = function
  | `WEXITED i -> Printf.sprintf "WEXITED %i" i
  | `WSIGNALED s -> Printf.sprintf "WSIGNALED %i" s
  | `WSTOPPED s -> Printf.sprintf "WSTOPPED %i" s

let string_of_sockaddr = function
  | Unix.ADDR_UNIX s -> s
  | Unix.ADDR_INET (ip, _) -> Unix.string_of_inet_addr ip

let end_job id sockaddr =
  let port, l = SHashtbl.find nodes (string_of_sockaddr sockaddr) in
  let j = List.find (fun (c : computation) -> c.id = id) l in
  SHashtbl.replace nodes
    (string_of_sockaddr sockaddr)
    ( port,
      CCList.remove_one ~eq:(fun (c1 : computation) c2 -> c1.id = c2.id) j l );
  Lwt.return_unit

let get_stats pass sockaddr =
  try%lwt
    Lwt_io.with_connection sockaddr (fun (ic, oc) ->
        Lwt_io.write_line oc (Serialization_j.string_of_query (pass, `STAT))
        >>= fun () ->
        Lwt_io.read_line ic >>= fun json ->
        match
          CCResult.guard (fun () -> Serialization_j.stat_of_string json)
        with
        | Ok (cpu, ram) ->
            Logs_lwt.debug (fun m -> m "Stat: cpu: %i ram: %i" cpu ram)
            >>= fun () -> Lwt.return (cpu, ram)
        | Error e ->
            Logs_lwt.err (fun m ->
                m "Error during the reception of the stats: %s"
                  (Printexc.to_string e))
            >|= fun () -> (0, 0))
  with e ->
    Logs_lwt.err (fun m ->
        m "Error during the collect of the stats: %s" (Printexc.to_string e))
    >|= fun () -> (0, 0)

let send_computation sockaddr pass computation =
  try%lwt
    Lwt_io.with_connection sockaddr (fun (ic, oc) ->
        Lwt_io.write_line oc
          (Serialization_j.string_of_query (pass, `COMPUTATION computation))
        >>= fun () ->
        Lwt_io.flush oc >>= fun () ->
        try%lwt
          Lwt_io.read_line ic >>= fun json ->
          match
            CCResult.guard (fun () -> Serialization_j.answer_of_string json)
          with
          | Ok answer -> (
              match answer with
              | `Ok ->
                  Logs_lwt.info (fun m ->
                      m "Computation %i,%i successfully sent to %s"
                        (fst computation.id) (snd computation.id)
                        (string_of_sockaddr sockaddr))
                  >|= fun () -> true
              | `Error s ->
                  Logs_lwt.err (fun m ->
                      m "Error when sending copmutation %i,%i: %s"
                        (fst computation.id) (snd computation.id) s)
                  >|= fun () -> false )
          | Error e ->
              Logs_lwt.err (fun m ->
                  m "Error during the reception of the answer: %s"
                    (Printexc.to_string e))
              >|= fun () -> false
        with End_of_file ->
          Logs_lwt.err (fun m -> m "Error during the read of the answer: EOF")
          >|= fun () -> false)
  with e ->
    Logs_lwt.err (fun m ->
        m "Error when sending a computation: %s" (Printexc.to_string e))
    >|= fun () -> false

let mutex = ref true

let rec launch_job server_port pass () =
  if !mutex && not (CCDeque.is_empty jobs) then (
    mutex := false;
    let job = CCDeque.peek_front jobs in
    SHashtbl.fold
      (fun addr (port, l) node ->
        node >>= fun node ->
        match node with
        | Some x -> Lwt.return (Some x)
        | None ->
            let sockaddr =
              Unix.ADDR_INET (Unix.inet_addr_of_string addr, port)
            in
            get_stats pass sockaddr >|= fun (cpu, ram) ->
            if cpu >= job.cpu && ram >= job.ram then Some (addr, port, l)
            else None)
      nodes (Lwt.return None)
    >>= fun node ->
    match node with
    | None ->
        mutex := true;
        Logs_lwt.debug (fun m -> m "No free node")
    | Some (addr, port, _) ->
        let sockaddr = Unix.ADDR_INET (Unix.inet_addr_of_string addr, port) in
        let computation =
          {
            id = (job.id, job.current);
            env =
              [
                Printf.sprintf "OCLUSTER_ARRAY_TASK_ID=%i" job.current;
                Printf.sprintf "OCLUSTER_TASK_ID=%i" job.id;
              ];
            script = job.script;
            args = job.args;
            time = job.time;
            port = server_port;
            cpu = job.cpu;
            ram = job.ram;
          }
        in
        if%lwt send_computation sockaddr pass computation then (
          ( SHashtbl.update nodes
              ~f:(fun _ e ->
                match e with
                | None -> Some (port, [ computation ])
                | Some (port, l) -> Some (port, computation :: l))
              ~k:addr;
            if job.current + 1 < job.iteration then
              job.current <- job.current + 1
            else CCDeque.remove_front jobs;
            Lwt.return_unit )
          >>= fun () ->
          Lwt_unix.sleep 4. >>= fun () ->
          mutex := true;
          launch_job server_port pass () ) )
  else Lwt.return_unit

let server_handler pass port sockaddr (ic, oc) =
  Lwt_io.read_line ic >>= fun json ->
  match CCResult.guard (fun () -> Serialization_j.query_of_string json) with
  | Result.Ok (query_pass, query) ->
      if query_pass = pass then
        match query with
        | `RESULT result ->
            Lwt_io.write_line oc (Serialization_j.string_of_answer `Ok)
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.info (fun m ->
                    m "Receive result: %s"
                      (Serialization_j.string_of_result
                         {
                           result with
                           stdout = "<stdout>";
                           stderr = "<stderr>";
                         }))
            >|= fun () ->
            Lwt.async (fun () ->
                Lwt.join
                  [
                    ( if String.length result.stdout > 0 then
                      Lwt_io.with_file ~mode:Lwt_io.output
                        (Printf.sprintf "ocluster_%i_%i.out" (fst result.id)
                           (snd result.id))
                        (fun oc -> Lwt_io.write oc result.stdout)
                    else Lwt.return_unit );
                    ( if String.length result.stderr > 0 then
                      Lwt_io.with_file ~mode:Lwt_io.output
                        (Printf.sprintf "ocluster_%i_%i.err" (fst result.id)
                           (snd result.id))
                        (fun oc -> Lwt_io.write oc result.stderr)
                    else Lwt.return_unit );
                    Lwt_io.with_file ~mode:Lwt_io.output
                      (Printf.sprintf "ocluster_%i_%i.log" (fst result.id)
                         (snd result.id))
                      (fun oc ->
                        Lwt_io.write oc
                          (Printf.sprintf
                             "Job completed at %f\nReturn code: %s\n"
                             (Unix.time ())
                             (string_of_ret_code result.ret_code)));
                    end_job result.id sockaddr;
                  ])
        | `JOB submission ->
            Lwt_io.write_line oc (Serialization_j.string_of_answer `Ok)
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.info (fun m ->
                    m "Receive submission: %s"
                      (Serialization_j.string_of_submission submission))
            >|= fun () ->
            let empty = CCDeque.is_empty jobs in
            let job =
              {
                id = !jobs_id;
                name = submission.name;
                current = 0;
                iteration = submission.iteration;
                script = submission.script;
                args = submission.args;
                time = submission.time;
                port;
                cpu = submission.cpu;
                ram = submission.ram;
              }
            in
            CCDeque.push_back jobs job;
            incr jobs_id;
            if empty then Lwt.async (launch_job port pass)
        | `JOBQ (id, delete) as jobq ->
            Lwt_io.write_line oc (Serialization_j.string_of_answer `Ok)
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.info (fun m ->
                    m "Receive JOBQ command: %s"
                      (Serialization_j.string_of_query_data jobq))
            >>= fun () ->
            if delete then (
              let last_id =
                try (CCDeque.peek_back jobs).id with CCDeque.Empty -> 0
              in
              let id = CCOpt.get_or ~default:last_id id in
              CCDeque.filter_in_place jobs (fun j -> j.id != id);
              Logs_lwt.info (fun m -> m "Removing jobs with id %i" id) )
            else
              let jobs_list =
                match id with
                | None -> CCDeque.to_list jobs
                | Some id ->
                    CCDeque.(
                      filter (fun (j : job) -> j.id = id) jobs |> to_list)
              in
              let running_list =
                SHashtbl.to_list nodes
                |>
                match id with
                | None ->
                    List.map (fun (ip, (_, computations)) -> (ip, computations))
                | Some id ->
                    List.map (fun (ip, (_, computations)) ->
                        ( ip,
                          List.filter_map
                            (fun (c : computation) ->
                              if fst c.id = id then Some c else None)
                            computations ))
              in
              Lwt_io.write_line oc
                (Serialization_j.string_of_jobs_status
                   (jobs_list, running_list))
              >>= fun () -> Lwt_io.flush oc
        | _ ->
            Lwt_io.write_line oc
              (Serialization_j.string_of_answer (`Error "Unwanted command"))
            >>= (fun () -> Lwt_io.flush oc)
            <&> Logs_lwt.warn (fun m -> m "Receive a unwanted command")
      else
        Lwt_io.write_line oc
          (Serialization_j.string_of_answer (`Error "Wrong password"))
        >>= (fun () -> Lwt_io.flush oc)
        <&> Logs_lwt.warn (fun m -> m "Wrong password: %s" query_pass)
  | Result.Error e ->
      Lwt_io.write_line oc
        (Serialization_j.string_of_answer
           (`Error
             (Printf.sprintf "Error during the reception of the computation: %s"
                (Printexc.to_string e))))
      >>= (fun () -> Lwt_io.flush oc)
      <&> Logs_lwt.err (fun m ->
              m "Error during the reception of the computation: %s"
                (Printexc.to_string e))

let rec timer delay f = Lwt_unix.sleep delay >>= f >>= fun () -> timer delay f

let get_running_jobs pass sockaddr =
  try%lwt
    Lwt_io.with_connection sockaddr (fun (ic, oc) ->
        Lwt_io.write_line oc (Serialization_j.string_of_query (pass, `RUNNING))
        >>= fun () ->
        Lwt_io.read_line ic >>= fun json ->
        match
          CCResult.guard (fun () -> Serialization_j.running_jobs_of_string json)
        with
        | Ok l -> Lwt.return l
        | Error e ->
            Logs_lwt.err (fun m ->
                m "Error during the reception of the running jobs: %s"
                  (Printexc.to_string e))
            >|= fun () -> [])
  with e ->
    Logs_lwt.err (fun m ->
        m "Error during the collect of the stats: %s" (Printexc.to_string e))
    >|= fun () -> []

let check_jobs pass () =
  SHashtbl.map_list
    (fun ip (port, computations) ->
      let sockaddr = Unix.ADDR_INET (Unix.inet_addr_of_string ip, port) in
      get_running_jobs pass sockaddr >>= fun running_jobs ->
      Lwt_list.iter_s
        (fun computation ->
          if not (List.mem computation running_jobs) then (
            CCDeque.push_front jobs
              {
                id = fst computation.id;
                name = "Restart computation";
                current = snd computation.id;
                iteration = snd computation.id + 1;
                script = computation.script;
                args = computation.args;
                time = computation.time;
                port = computation.port;
                cpu = computation.cpu;
                ram = computation.ram;
              };
            Lwt.return_unit )
          else Lwt.return_unit)
        computations
      >|= fun () ->
      SHashtbl.update nodes
        ~f:(fun _ e ->
          match e with
          | None -> Some (port, [])
          | Some (port, l) ->
              Some (port, List.filter (fun e -> List.mem e running_jobs) l))
        ~k:ip)
    nodes
  |> Lwt.join

let stop_server resolver server _ = Lwt.wakeup_later resolver server

let cmd config () =
  let conf =
    Serialization_j.master_conf_of_string (CCIO.with_in config CCIO.read_all)
  in
  List.iter (fun n -> SHashtbl.add nodes n.addr (n.port, [])) conf.nodes;
  let promise, resolver = Lwt.task () in
  Logs_lwt.info (fun m -> m "master at %i with pass %s" conf.port conf.pass)
  >>= fun () ->
  ( Lwt.async (fun () -> timer 180. (launch_job conf.port conf.pass));
    Lwt.async (fun () -> timer 1800. (check_jobs conf.pass));
    Lwt_io.establish_server_with_client_address
      (Unix.ADDR_INET (Unix.inet_addr_any, conf.port))
      (server_handler conf.pass conf.port) )
  >>= fun server ->
  let _ = Lwt_unix.on_signal 15 (stop_server resolver ())
  and _ = Lwt_unix.on_signal 2 (stop_server resolver ()) in
  promise >>= fun () ->
  Lwt_io.shutdown_server server
  <&> Logs_lwt.info (fun m -> m "Shuting down server")
