open Lwt.Infix
open Serialization_t

let cmd id delete addr port pass () =
  Lwt_io.with_connection
    (Lwt_unix.ADDR_INET (Unix.inet_addr_of_string addr, port))
    (fun (ic, oc) ->
      Lwt_io.write_line oc
        (Serialization_j.string_of_query (pass, `JOBQ (id, delete)))
      >>= fun () ->
      Lwt_io.read_line ic >>= fun json ->
      match
        CCResult.guard (fun () -> Serialization_j.answer_of_string json)
      with
      | Ok answer -> (
          match answer with
          | `Ok ->
              Logs_lwt.info (fun m -> m "Jobq query successfully sent.")
              >>= fun () ->
              if not delete then
                Lwt_io.read_line ic >>= fun json ->
                let open PrintBox in
                match
                  CCResult.guard (fun () ->
                      Serialization_j.jobs_status_of_string json)
                with
                | Ok (jobs, running) ->
                    let b_jobs =
                      Array.of_list jobs |> fun jobs ->
                      Array.init
                        (Array.length jobs + 1)
                        (fun i ->
                          if i = 0 then
                            [|
                              text "ID";
                              text "Name";
                              text "Iteration(current)";
                              text "ARG";
                            |]
                          else
                            [|
                              sprintf "%i" jobs.(i - 1).id;
                              text jobs.(i - 1).name;
                              sprintf "%i(%i)" jobs.(i - 1).iteration
                                jobs.(i - 1).current;
                              text (String.concat " " jobs.(i - 1).args);
                            |])
                      |> grid |> frame
                    in
                    let b_running =
                      Array.of_list running |> fun running ->
                      Array.init
                        (Array.length running + 1)
                        (fun i ->
                          if i = 0 then [| text "Server"; text "Computations" |]
                          else
                            let ip, computations = running.(i - 1) in
                            [|
                              sprintf "%s" ip;
                              ( Array.of_list computations |> fun computations ->
                                Array.init
                                  (Array.length computations + 1)
                                  (fun i ->
                                    if i = 0 then [| text "ID"; text "ARG" |]
                                    else
                                      [|
                                        sprintf "%i, %i"
                                          (fst computations.(i - 1).id)
                                          (snd computations.(i - 1).id);
                                        text
                                          (String.concat " "
                                             computations.(i - 1).args);
                                      |])
                                |> grid );
                            |])
                      |> grid |> frame
                    in
                    Format.printf "Jobq:@,%a@.@.Running:@,%a@." PrintBox_text.pp
                      b_jobs PrintBox_text.pp b_running;
                    Lwt.return_unit
                | Error e ->
                    Logs_lwt.err (fun m ->
                        m "Error during the reception of the answer: %s"
                          (Printexc.to_string e))
              else Lwt.return_unit
          | `Error s ->
              Logs_lwt.err (fun m ->
                  m "Error during the reception of the jobq query: %s" s) )
      | Error e ->
          Logs_lwt.err (fun m ->
              m "Error during the reception of the answer: %s"
                (Printexc.to_string e)))
