open Cmdliner

let lwt_reporter () =
  let buf_fmt ~like =
    let b = Buffer.create 512 in
    ( Fmt.with_buffer ~like b
    , fun () ->
        let m = Buffer.contents b in
        Buffer.reset b ; m )
  in
  let app, app_flush = buf_fmt ~like:Fmt.stdout in
  let dst, dst_flush = buf_fmt ~like:Fmt.stderr in
  let reporter = Logs_fmt.reporter ~app ~dst () in
  let report src level ~over k msgf =
    let k () =
      let write () =
        match level with
        | Logs.App ->
            Lwt_io.write Lwt_io.stdout (app_flush ())
        | _ ->
            Lwt_io.write Lwt_io.stderr (dst_flush ())
      in
      let unblock () = over () ; Lwt.return_unit in
      Lwt.async (fun () -> Lwt.finalize write unblock) ;
      k ()
    in
    reporter.Logs.report src level ~over:(fun () -> ()) k msgf
  in
  {Logs.report}

let setup_log style_renderer level =
  Fmt_tty.setup_std_outputs ?style_renderer () ;
  Logs.set_level level ;
  Logs.set_reporter (lwt_reporter ()) ;
  ()

let setup_log =
  Term.(const setup_log $ Fmt_cli.style_renderer () $ Logs_cli.level ())

let node_cmd =
  let config =
    let doc = "Configuration of the node" in
    Arg.(required & pos 0 (some string) None & info [] ~docv:"CONF" ~doc)
  in
  let doc = "Start un computational node" in
  let exits = Term.default_exits in
  let man =
    [ `S Manpage.s_description
    ; `P
        "Start un computational node on the given port with the given password"
    ]
  in
  ( Term.(const Lwt_main.run $ (const Node.cmd $ config $ setup_log))
  , Term.info "node" ~doc ~sdocs:Manpage.s_common_options ~exits ~man )

let master_cmd =
  let config =
    let doc = "Configuration file of the master" in
    Arg.(required & pos 0 (some file) None & info [] ~docv:"CONF_FILE" ~doc)
  in
  let doc = "Start un master server" in
  let exits = Term.default_exits in
  let man =
    [ `S Manpage.s_description
    ; `P "Start un master server on the given port with the given password" ]
  in
  ( Term.(const Lwt_main.run $ (const Master.cmd $ config $ setup_log))
  , Term.info "master" ~doc ~sdocs:Manpage.s_common_options ~exits ~man )

let submit_cmd =
  let cpu =
    let doc = "Number of cpu needed" in
    Arg.(value & opt int 1 & info ["c"; "cpu"] ~docv:"CPU" ~doc)
  in
  let ram =
    let doc = "Quantity of RAM needed" in
    Arg.(value & opt int 1024 & info ["r"; "ram"] ~docv:"RAM" ~doc)
  in
  let time =
    let doc = "Time limit" in
    Arg.(value & opt (some float) None & info ["t"; "time"] ~docv:"TIME" ~doc)
  in
  let iteration =
    let doc = "Number of iteration" in
    Arg.(value & opt int 1 & info ["i"; "iteration"] ~docv:"N" ~doc)
  in
  let port =
    let doc = "port on which master listen" in
    Arg.(value & opt int 4242 & info ["p"; "port"] ~docv:"PORT" ~doc)
  in
  let script =
    let doc = "Ocluster script" in
    Arg.(required & pos 0 (some file) None & info [] ~docv:"SCRIPT" ~doc)
  in
  let pass =
    let doc = "Password of the node" in
    Arg.(required & pos 1 (some string) None & info [] ~docv:"PASS" ~doc)
  in
  let addr =
    let doc = "Ip of the master" in
    Arg.(required & pos 2 (some string) None & info [] ~docv:"IP" ~doc)
  in
  let args =
    let doc = "Arguments passed to the script" in
    Arg.(value & pos_right 2 string [] & info [] ~docv:"ARGS" ~doc)
  in
  let doc = "Submit jobs to the cluster" in
  let exits = Term.default_exits in
  let man = [`S Manpage.s_description; `P "Submit jobs to the cluster"] in
  ( Term.(
      const Lwt_main.run
      $ ( const Submit.cmd $ cpu $ ram $ time $ iteration $ port $ script $ pass
        $ addr $ args $ setup_log ))
  , Term.info "submit" ~doc ~sdocs:Manpage.s_common_options ~exits ~man )

let jobq_cmd =
  let id =
    let doc = "Id of the job to consider" in
    Arg.(value & opt (some int) None & info ["i"; "id"] ~docv:"ID" ~doc)
  in
  let delete =
    let doc = "Delete a job. If job id is given delete the last job." in
    Arg.(value & flag & info ["d"; "delete"] ~doc)
  in
  let port =
    let doc = "port on which master listen" in
    Arg.(value & opt int 4242 & info ["p"; "port"] ~docv:"PORT" ~doc)
  in
  let pass =
    let doc = "Password of the node" in
    Arg.(required & pos 0 (some string) None & info [] ~docv:"PASS" ~doc)
  in
  let addr =
    let doc = "Ip of the master" in
    Arg.(required & pos 1 (some string) None & info [] ~docv:"IP" ~doc)
  in
  let doc = "Interact with the job queue." in
  let exits = Term.default_exits in
  let man = [`S Manpage.s_description; `P "Interact with the job queue."] in
  ( Term.(
      const Lwt_main.run
      $ (const Jobq.cmd $ id $ delete $ addr $ port $ pass $ setup_log))
  , Term.info "jobq" ~doc ~sdocs:Manpage.s_common_options ~exits ~man )

let default_cmd =
  let doc = "Use a pool of computer as a cluster." in
  let man =
    [ `S Manpage.s_bugs
    ; `P "Email bug reports to <fardale+ocluster at crans.org>." ]
  in
  let sdocs = Manpage.s_common_options in
  let exits = Term.default_exits in
  ( Term.(ret (const (fun _ -> `Help (`Pager, None)) $ const ()))
  , Term.info "ocluster" ~version:"0.1.4" ~doc ~sdocs ~exits ~man )

let () =
  Term.(
    exit @@ eval_choice default_cmd [node_cmd; master_cmd; submit_cmd; jobq_cmd])
